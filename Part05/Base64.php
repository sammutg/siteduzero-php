<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>base64</title>
    </head>
    <body>

		<h1>base64_encode</h1>
 		<?php
		$str = 'www.test.fr';
		echo base64_encode($str);
		?>

		<h1>base64_decode</h1>
 		<?php
		$str = 'VGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw==';
		echo base64_decode($str);
		?>

    </body>
</html>