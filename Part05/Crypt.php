<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crypt</title>
    </head>
    <body>

		<h1>Crypter un mot pour htpassword</h1>

		<p>
		<?php
		if (isset($_POST['login']) AND isset($_POST['pass']))
		{
		    $login = $_POST['login'];
		    $pass_crypte = crypt($_POST['pass']); // On crypte le mot de passe

		    echo 'Ligne à copier dans le .htpasswd :<br />' . $login . ':' . $pass_crypte;
		}

		else // On n'a pas encore rempli le formulaire
		{
		?>
		</p>

		<p>Entrez votre login et votre mot de passe pour le crypter.</p>

		<form method="post">
		    <p>
		        Login : <input type="text" name="login"><br />
		        Mot de passe : <input type="text" name="pass"><br /><br />

		        <input type="submit" value="Crypter !">
		    </p>
		</form>

		<?php
		}
		?>

    </body>
</html>