<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Fonctions</title>
    </head>
    <body>
	
		<h1>Les Fonctions</h1>

		<h2>sort($prenoms)</h2>
		<?php
		// La fonction array permet de créer un array
		$prenoms = array('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');
		
		// La fonction sort trie un tableau
		sort($prenoms);

		foreach($prenoms as $element)
		{
		    echo $element . '<br />';
		}
		?>

		<?php
		$fruits = array("lemon", "orange", "banana", "apple");
		sort($fruits);
		foreach ($fruits as $key => $val) {
		    echo "fruits[" . $key . "] = " . $val . "<br />\n";
		}
		echo $fruits[1] . "<br />\n";
		?>

		<h2>mt_rand ()</h2>
		<ol>
		<li>
			int mt_rand ( void )
			<?php echo mt_rand(); ?>
		</li>
		<li>
			int mt_rand ( int $min, int $max )
			<?php echo mt_rand(5,15); ?>
		</li>
		</ol>

		<p>Certains paramètres sont obligatoires, d'autres pas ils sont entre crochets</p>
		<code>string date ( string format [, int timestamp])</code><br />

		<?php
			echo "date du jour : " . date('d/m/Y');
		?>
		
    </body>
</html>