<?php
//  Pour permettre l'héritage, il est impératif d'inclure le fichier Membre.class.php au préalable.
include_once('Membre.class.php');

class Admin extends Membre
{
    private $couleur;

    public function setCouleur()
    {
        // ...
    }
    
    public function getCouleur()
    {
        // ...
    }
}
?>