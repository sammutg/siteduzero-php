<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les expressions régulières</title>
    </head>
    <body>
	
		<h1>Une histoire de métacaractères</h1>
		
		<pre><strong># ! ^ $ ( ) [ ] { } ? + * . \ |</strong></pre>

		<p><strong>#Quoi \?#</strong> : Quoi ?</p>
		<?php
		$regex_quoi = 'Quoi ?';
		if (preg_match("#\?#", $regex_quoi))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<h2>Les classes abrégées</h2>
		<ul>
			<li><strong>\d</strong>	Indique un chiffre. Ca revient exactement à taper [0-9]</li>
			<li><strong>\D</strong>	Indique ce qui n'est PAS un chiffre. Ca revient à taper [^0-9]</li>
			<li><strong>\w</strong>	Indique un mot. Cela correspond à taper [a-zA-Z0-9_]</li>
			<li><strong>\W</strong>	Indique ce qui n'est PAS un mot. Si vous avez suivi, ça revient à taper [^a-zA-Z0-9_]</li>
			<li><strong>\t</strong>	Indique une tabulation</li>
			<li><strong>\n</strong>	Indique une nouvelle ligne</li>
			<li><strong>\r</strong>	Indique un retour chariot</li>
			<li><strong>\s</strong>	Indique un espace blanc (correspond à \t \n \r)</li>
			<li><strong>\S</strong>	Indique ce qui n'est PAS un espace blanc (\t \n \r)</li>
			<li><strong>. </strong> Le point indique n'importe quel caractère ! Il autorise donc tout !</li>
		</ul>

		<h2>Un numéro de téléphone</h2>
		<p>
		<?php
		if (isset($_POST['telephone']))
		{
			$_POST['telephone'] = htmlspecialchars($_POST['telephone']); // On rend inoffensives les balises HTML que le visiteur a pu rentrer

			if (preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $_POST['telephone']))
			{
				echo 'Le ' . $_POST['telephone'] . ' est un numéro <strong>valide</strong> !';
			}
			else
			{
				echo 'Le ' . $_POST['telephone'] . ' n\'est pas valide, recommencez !';
			}
		}
		?>
		</p>

		<form method="post">
		<p>
			<label for="telephone">Votre téléphone ?</label>
			<input id="telephone" name="telephone" /><br />
			<input type="submit" value="Vérifier le numéro" />
		</p>
		</form>


		<h2>Une adresse e-mail</h2>
		<p>
		<?php
		if (isset($_POST['mail']))
		{
		$_POST['mail'] = htmlspecialchars($_POST['mail']); // On rend inoffensives les balises HTML que le visiteur a pu rentrer

		    if (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['mail']))
		    {
		        echo 'L\'adresse ' . $_POST['mail'] . ' est <strong>valide</strong> !';
		    }
		    else
		    {
		        echo 'L\'adresse ' . $_POST['mail'] . ' n\'est pas valide, recommencez !';
		    }
		}
		?>
		</p>

		<form method="post">
		<p>
		    <label for="mail">Votre mail ?</label> <input id="mail" name="mail" /><br />
		    <input type="submit" value="Vérifier le mail" />
		</p>
		</form>

    </body>
</html>