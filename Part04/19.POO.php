<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Programmation Orientée Objet</title>
    </head>
    <body>
	
		<h1>Programmation Orientée Objet</h1>
		
		
		<h2>Créer un objet à partir de la classe</h2>
		<?php
		include_once('19.POO/Membre.class.php');

		//$membre = new Membre(1); // Le membre n°32 est chargé !
		// $membre->setPseudo('Guillaume');

		//echo 'Salut, ' . $membre->getPseudo() ;
		?>

		<h2>Constructeur, destructeur et autres fonctions spéciales</h2>

		<?php
			// $membre = new Membre(32); // Le membre n°32 est chargé !
			// echo $membre->getPseudo() ;
		 ?>

		<h2>Héritage</h2>

		<p><strong>Il y a héritage quand on peut dire : "A est un B"</strong></p>

		<?php
		// $membre = new Membre(31); // Contient un pseudo, un e-mail...
		// $maitreDesLieux = new Admin(2); // Contient les mêmes données qu'un membre + la couleur
		// 
		// $membre->setPseudo('Arckintox'); // OK
		// $maitreDesLieux->setPseudo('M@teo21'); // OK
		// 
		// $membre->setCouleur('Rouge'); // Impossible (un membre n'a pas de couleur)
		// $maitreDesLieux->setCouleur('Rouge'); // OK
		?>

		<h2>Encapsulation</h2>

		<?php
			$membre = new Membre(4);
			$membre->setPseudo('M@teo21'); // OK car setPseudo est public
			$membre->pseudo = 'M@teo21'; // Interdit car $pseudo est private
			
			echo 'Salut, ' . $membre->getPseudo() ;
			
		?>

    </body>
</html>