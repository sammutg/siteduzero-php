<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les expressions régulières</title>
    </head>
    <body>
	
		<h1>Les expressions régulières</h1>

		<p>2 types d'expressions régulières : POSIX et PCRE</p>

		<h2>PCRE</h2>
		<p>Expressions plus rapides et performantes</p>

		<h3>PREG_MATCH</h3>
		<?php 
		$regex = 'J\'aime jouer de la guitare.';
		echo '<strong>#Guitare#</strong> : ' . $regex . '" contient #Guitare#</p>';

		if (preg_match("#guitare#i", $regex))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<h3>LE SYMBOLE OU</h3>

		<p><strong>#guitare|piano#</strong> : J'aime jouer de la guitare</p>
		<?php
		$regex = 'J\'aime jouer de la guitare.';
		if (preg_match("#guitare|piano#", $regex))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<H3>DÉBUT ET FIN DE CHAÎNE</H3>
		<ul>
			<li>^ (accent circonflexe) : indique le début d'une chaîne.</li>
			<li>$ (dollar) : indique la fin d'une chaîne.</li>
		</ul>
		
		<p><strong>#^Bonjour#</strong> : Bonjour, je joue de la guitare et du banjo.</p>
		<?php
		$regex_bonjour = 'Bonjour, je joue de la guitare et du banjo';
		if (preg_match("#^Bonjour#", $regex_bonjour))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#banjo$#</strong> : Bonjour, je joue de la guitare et du banjo.</p>
		<?php
		if (preg_match("#banjo$#", $regex_bonjour))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<H3>LES CLASSES DE CARACTÈRES</H3>
		<H4>DES CLASSES SIMPLES</H4>
		
		<p><strong>#gr[io]s#</strong> : Bonjour, je joue de la guitare et du gros banjo.</p>
		<?php
		$regex_grs = 'Bonjour, je joue de la guitare et du gros banjo';
		if (preg_match("#gr[io]s#", $regex_grs))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#^[jaeiouy]#i</strong> : Je suis un vrai zéro.</p>
		<?php
		$regex_zero = 'Je suis un vrai zéro';
		if (preg_match("#^[jaeiouy]#i", $regex_zero))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<H4>LES INTERVALLES DE CLASSE</H4>
		<p><strong>#[a-z0-9]#</strong> : 1 vrai zéro je suis</p>

		<?php
		$regex_unzero = '1 vrai zéro je suis';
		if (preg_match("#[a-z0-9]#", $regex_unzero))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>
		
		<p><strong>#[A-Z0-9]#</strong> : cette phrase ne comporte pas de majuscule ni de chiffre</p>
		<?php
		$regex_maj = 'cette phrase ne comporte pas de majuscule ni de chiffre';
		if (preg_match("#[A-Z0-9]#", $regex_maj))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#&lt;h[1-6]&gt;#</strong> : &lt;h1&gt;Une balise de titre HTML&lt;/h1&gt;</p>
		<?php
		$regex_h = '<h1>Une balise de titre HTML</h1>';
		if (preg_match("#<h[1-6]>#", $regex_h))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<h4>DIRE QU'ON EN VEUX PAS</h4>
		<p><strong>#[^0-9]#</strong> : Cette phrase contient autre chose que des chiffres</p>
		<?php
		$regex_negatif = 'Cette phrase contient autre chose que des chiffres';
		if (preg_match("#[^0-9]#", $regex_negatif))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#[^aeiouy]$#</strong> : Cette phrase ne se termine pas par une voyelle</p>	
		<?php
		$regex_voyelle = 'Cette phrase ne se termine pas par une voyelle';
		if (preg_match("#[^aeiouy]$#", $regex_voyelle))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<H3>LES QUANTIFICATEURS</H3>

		<h4>SYMBOLES LES PLUS COURANTS</h4>
		<ul>
			<li><strong>?</strong> #a?# : ce symbole indique que la lettre est facultative. Elle peut y être 0 ou 1 fois.</li>
			<li><strong>+</strong> #a+# : la lettre est obligatoire. Elle peut apparaître 1 ou plusieurs fois.</li>
			<li><strong>*</strong> #a*# : la lettre est facultative. Elle peut apparaître 0, 1 ou plusieurs fois.</li>
		</ul>

		<p><strong>#bor?is#</strong> : cette phrase contient bois ou boris</p>
		<?php
		$regex_boris = 'cette phrase contient bois ou boris';
		if (preg_match("#bor?is#", $regex_boris))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#Ay(ay)*#</strong> : Ayayayayayayayayayayayoy</p>
		<?php
		$regex_ay = 'Ayayayayayayayayayayayoy';
		if (preg_match("#Ay(ay|oy)*#", $regex_ay))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#^Yaho+$#</strong> : Yahoooooo c'est génial !</p>
		<?php
		$regex_yahoo = 'Yahoooooo c\'est génial !';
		if (preg_match("#^Yaho+$#", $regex_yahoo))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>


		<H4>PLUS PRÉCIS GRÂCE AUX ACCOLADES</H4>
		<ul>
			<li><strong>?</strong> = {0,1} </li>
			<li><strong>+</strong> = {1,} </li>
			<li><strong>*</strong> = {0,} </li>
		</ul>

		<p><strong>#e{2,}#</strong> : Beeeeeeeeeelle</p>
		<?php
		$regex_e = 'Beeeeeeeeeelle';
		if (preg_match("#e{2,}#", $regex_e))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#^Bla (bla){4}$#</strong> : Bla blablablabla</p>
		<?php
		$regex_bla = 'Bla blablablabla';
		if (preg_match("#^Bla (bla){4}$#", $regex_bla))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>

		<p><strong>#^[0-9]{6}$#</strong> : 123456</p>
		<?php
		$regex_six = '123456';
		if (preg_match("#^[0-9]{6}$#", $regex_six))
		{ echo 'Vrai'; } else { echo 'Faux';}
		?>


    </body>
</html>