<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert, Update, delete</title>
    </head>
    <body>

		<h1>Insert, Update, Delete</h1>
		<h2>INSERT : ajouter des données depuis un formulaire</h2>

		<form method="post" action="11.Insert3.php">
			<fieldset>
				<legend>Veuillez renseigner les infos suivantes :</legend>
				<p>
					<label for="nom">Nom du jeu</label>
					<input type="text" name="nom" id="nom" />
				</p>
				<p>
					<label for="possesseur">Possesseur</label>
					<input type="text" name="possesseur" id="possesseur" />
				</p>
				<p>
	 				<label for="console">Console</label>
	 				<select name="console" id="console">
						<option value="XBox">XBox</option>
						<option value="PS2">PS2</option>
						<option value="Ds">Ds</option>
						<option value="Dreamcast">Dreamcast</option>
						<option value="NES">NES</option>
						<option value="Gamecube">Gamecube</option>
						<option value="iPhone">iPhone</option>
	 				</select>
				</p>
				<p>
					<label for="prix">Prix</label> 
					<input id="prix" name="prix" type="number" min="1" max="100">
				</p>
				<p>
					<label for="nbre_joueurs_max">Nombre de joueurs maximums</label> 
					<input id="nbre_joueurs_max" name="nbre_joueurs_max" type="number" min="1" max="50">
				</p>
				<p>
					<label for="commentaires">Commentaire</label>
					<textarea name="commentaires" id="commentaires" rows="8" cols="45"></textarea>
				</p>

			</fieldset>

			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

    </body>
</html>