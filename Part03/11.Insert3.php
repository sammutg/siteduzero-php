<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert, Update, delete</title>
    </head>
    <body>

	<!-- =============================================================== -->
	<!-- = INSERTION DE DONNÉES VARIABLES GRÂCE À UNE REQUÊTE PRÉPARÉE = -->
	<!-- =============================================================== -->
		<?php
		try
		{
			// Connexion MySQL sécurisée
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			$req = $bdd->prepare('INSERT INTO jeux_video(nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(:nom, :possesseur, :console, :prix, :nbre_joueurs_max, :commentaires)');
			$req->execute(array(
				'nom' => strip_tags($_POST['nom']),
				'possesseur' => strip_tags($_POST['possesseur']),
				'console' => strip_tags($_POST['console']),
				'prix' => strip_tags($_POST['prix']),
				'nbre_joueurs_max' => strip_tags($_POST['nbre_joueurs_max']),
				'commentaires' => strip_tags($_POST['commentaires'])
				));

			echo 'Le jeu a bien été ajouté !';

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>