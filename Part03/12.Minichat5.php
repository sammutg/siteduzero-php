<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Chat</title>
    </head>
    <body>

	<!-- ============================================= -->
	<!-- = PAGE POUR AFFICHER SEULEMENT LES MESSAGES = -->
	<!-- ============================================= -->
		<?php
		try
		{
			require ("12.Connexion.php");

		    // On récupère tout le contenu de la table minichat
		    $reponse = $bdd->query('SELECT * FROM minichat ORDER BY ID DESC');
		
		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
			<p>
				<strong><?php echo htmlspecialchars($donnees['pseudo']); ?></strong> : 
				<em><?php echo htmlspecialchars($donnees['message']); ?></em>
			</p>
		    <?php
		    }

		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>