<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert, Update, delete</title>
    </head>
    <body>
	
		<h1>Insert, Update, Delete</h1>

		<h2>UPDATE : modifier des données</h2>


		<p><strong>exec()</strong> UPDATE</p>
		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// $bdd->exec('UPDATE jeux_video SET nom = \'Battlefield\' WHERE nom = \'Battlefield 1942\'');
			$nb_modifs = $bdd->exec('UPDATE jeux_video SET nom = \'Battlefield 1942\' WHERE nom = \'Battlefield\'');
			echo $nb_modifs . ' entrées ont été modifiées !';

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>