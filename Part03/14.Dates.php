<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Chat</title>
    </head>
    <body>
	
		<h1>TP : Mini-Chat</h1>

		<form action="14.Dates2.php" method="post">
			<fieldset id="" class="">
				<p>
					<label for="pseudo">Pseudo</label>

					<?php
					// TEST DU PSEUDO
					// Si il existe on affiche une valeur dans le champs texte
					if(isset($_COOKIE['pseudo']))
					{
					?>
						
						<input type="text" name="pseudo" id="pseudo" value="<?php echo $_COOKIE['pseudo']; ?>" />
					<?php
					}
					else // Sinon on affiche un champ vide
					{
					?>
						<input type="text" name="pseudo" id="pseudo" />
					<?php
					}
					?>
				</p>
				<p>
					<label for="message">Votre message</label>
					<textarea name="message" id="message" rows="8" cols="45"></textarea>
				</p>
			</fieldset>
			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

		<h2>Les derniers messages de 2011</h2>
		<a href="14.Dates.php">Rafraîchir les messages</a>

		<p>Messages compris entre le 1er janvier et le 06 février 2011</p>
		<?php
		try
		{
			require ("12.Connexion.php");

		    // On récupère tout le contenu de la table minichat
		    // $reponse = $bdd->query('SELECT * FROM minichat WHERE date_creation >= \'2011-01-01 00:00:00\' AND date_creation <= \'2011-02-06 00:00:00\'');
		    $reponse = $bdd->query('SELECT * FROM minichat WHERE date_creation BETWEEN \'2011-01-01 00:00:00\' AND \'2011-02-06 00:00:00\'');

		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
			<p>
				<strong><?php echo htmlspecialchars($donnees['pseudo']); ?></strong> : 
				<em><?php echo htmlspecialchars($donnees['message']); ?></em>
			</p>
		    <?php
		    }

		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>
		
		
		<h2>Message avec l'heure</h2>
		<?php
		try
		{
			require ("12.Connexion.php");

		    // On récupère tout le contenu de la table minichat
		    $reponse = $bdd->query('SELECT pseudo, message, DATE_FORMAT(date_creation, \'%d/%m/%Y %Hh%imin%ss\') AS date_creation FROM minichat ORDER BY date_creation DESC');

		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
			<p>
				<?php echo $donnees['date_creation']; ?><br />
				<strong><?php echo htmlspecialchars($donnees['pseudo']); ?></strong> : 
				<em><?php echo htmlspecialchars($donnees['message']); ?></em>
			</p>
		    <?php
		    }

		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


    </body>
</html>