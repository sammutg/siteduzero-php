<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Blog</title>
    </head>
    <body>
	
		<h1>TP : Mini-Blog</h1>
		<h2>Les 5 derniers billets</h2>
		<hr />
		
		<?php
		try
		{
			require ("12.Connexion.php");

		    // On récupère tout le contenu de la table zero_billet
		    $reponse = $bdd->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y\' ) AS date_creation FROM zero_billet ORDER BY date_creation DESC LIMIT 0,5');

			echo "<ol>";
		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
				<li>
					<h3>
						<?php echo $donnees['titre']; ?>
						<br />
						<small>Le <strong><?php echo $donnees['date_creation']; ?></strong></small>
					</h3>
					<p>
						<?php echo nl2br(htmlspecialchars($donnees['contenu'])); ?>
					</p>
					<p><em><a href="15.Blog2.php?billet=<?php echo $donnees['id']; ?>">Lire les commentaires</a></em></p>
				</li>
		    <?php
		    }
		    $reponse->closeCursor(); // Termine le traitement de la requête
			echo "</ol>";

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>