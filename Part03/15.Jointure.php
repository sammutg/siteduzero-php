<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les jointures entre tables</title>
    </head>
    <body>
	
		<h1>Les jointures entre tables</h1>

		<ul>
			<li><strong>une jointure interne</strong> : force les données d'une table à avoir une correspondance dans l'autre.</li>
			<li><strong>une jointure externe</strong> : renvoi toutes les données d'une table même s'il n'y a pas de correspondance dans l'autre table</li>
		</ul>

		<h2>Les jointures internes</h2>
		<?php
		try
		{
			require ("12.Connexion.php");

		    // Requete
			echo "<p><strong>WHERE</strong> (obsolète)</p>";
			echo "<p>Cette requête récupére le nom du jeu et le prénom du propriétaire</p>";
		    $reponse = $bdd->query(
			'SELECT jeux_video.nom, proprietaires.prenom 
			FROM proprietaires, jeux_video
			WHERE jeux_video.id_proprietaire = proprietaires.id
			LIMIT 0,10');

			// On affiche chaque entrée une à une
			while ($donnees = $reponse->fetch())
			{
			?>
			<strong><?php echo $donnees['nom']; ?></strong> appartient à <?php echo $donnees['prenom']; ?><br />
			<?php
			}
			$reponse->closeCursor(); // Termine le traitement de la requête


			echo "<p>La même avec des alias</p>";
		    $reponse = $bdd->query(
			'SELECT j.nom AS nom_jeu, p.prenom AS prenom_proprietaire
			FROM proprietaires AS p, jeux_video AS j
			WHERE j.id_proprietaire = p.id
			LIMIT 0,10');

			// On affiche chaque entrée une à une
			while ($donnees = $reponse->fetch())
			{
			?>
			<strong><?php echo $donnees['nom_jeu']; ?></strong> appartient à <?php echo $donnees['prenom_proprietaire']; ?><br />
			<?php
			}
			$reponse->closeCursor(); // Termine le traitement de la requête

			echo "<p><strong>JOIN</strong></p>";
		    $reponse = $bdd->query(
			'SELECT j.nom AS nom_jeu, p.prenom AS prenom_proprietaire
			FROM proprietaires AS p
			INNER JOIN jeux_video AS j
			ON j.id_proprietaire = p.ID
			LIMIT 0,10');

			// On affiche chaque entrée une à une
			while ($donnees = $reponse->fetch())
			{
			?>
			<?php echo $donnees['nom_jeu']; ?></strong> appartient à <?php echo $donnees['prenom_proprietaire']; ?><br />
			<?php
			}
			$reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


		<h2>Les jointures externes</h2>
		<?php
		try
		{

			echo "<p><strong>LEFT JOIN</strong></p>";
		    $reponse = $bdd->query(
			'SELECT j.nom AS nom_jeu, p.prenom AS prenom_proprietaire
			FROM proprietaires AS p
			LEFT JOIN jeux_video AS j
			ON j.id_proprietaire = p.ID
			ORDER BY id_proprietaire');

			// On affiche chaque entrée une à une
			while ($donnees = $reponse->fetch())
			{
			?>
			<?php echo $donnees['prenom_proprietaire']; ?> possède <?php echo $donnees['nom_jeu']; ?><br />
			<?php
			}
			$reponse->closeCursor(); // Termine le traitement de la requête

			echo "<p><strong>RIGHT JOIN</strong></p>";
		    $reponse = $bdd->query(
			'SELECT j.nom AS nom_jeu, p.prenom AS prenom_proprietaire
			FROM proprietaires AS p
			RIGHT JOIN jeux_video AS j
			ON j.id_proprietaire = p.ID');

			// On affiche chaque entrée une à une
			while ($donnees = $reponse->fetch())
			{
			?>
			<?php echo $donnees['prenom_proprietaire']; ?> possède <?php echo $donnees['nom_jeu']; ?><br />
			<?php
			}
			$reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


    </body>
</html>