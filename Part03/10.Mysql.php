<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stocker des informations dans une base</title>
    </head>
    <body>
	
		<h1>Lire des données</h1>
		<h2>Se connecter à MySQL avec PDO</h2>
		
		<p><strong>Accès à la base</strong></p>
		<p>En cas d'erreur, php affiche toute la ligne avec les accès </p> 
		<?php
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root');
			// Cette ligne de code crée un objet $bdd qui représente la connexion à la base de données
			// @bdd n'est pas une variable même si cela y ressemble
		?>

		<p><strong>Tester les erreurs d'accès à la base</strong></p>
		<?php
		try
		{
			// On se connecte à MySQL
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options); 
		}
		catch (Exception $e)
		{
			// En cas d'erreur précédemment, on affiche un message et on arrête tout
			die('Erreur : '.$e->getMessage());
		}
		?>

		<h2>Récupérer les données</h2>

		<?php
		try
		{
		    // On se connecte à MySQL
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// Pour que tout le reste s'affiche en UTF-8.
			$bdd->query("SET NAMES 'utf8'");

		    // On récupère tout le contenu de la table jeux_video
		    $reponse = $bdd->query('SELECT * FROM jeux_video');

		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
		
			<p>
				<strong><?php echo $donnees['nom']; ?></strong><br />
				Possesseur : <?php echo $donnees['possesseur']; ?>, 
				Prix : <?php echo $donnees['prix']; ?>&euro;<br />
				jeu  <?php echo $donnees['console']; ?> - joueurs <?php echo $donnees['nbre_joueurs_max']; ?> au maximum<br />
				<?php echo 'commentaires de ' . $donnees['possesseur']; ?> : 
				<br /><em><?php echo $donnees['commentaires']; ?></em>
			</p>
		    <?php
		    }

		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    // En cas d'erreur précédemment, on affiche un message et on arrête tout
			echo '<h2>Une erreur est survenue, merci de contacter l\'administrateur du site</h2>';
			die('Erreur : '.$e->getMessage());
		}

		?>

		<h2>Les critères de sélection</h2>

		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// Astuce pour que tout le reste s'affiche en UTF-8.
			$bdd->query("SET NAMES 'utf8'");

		    // $reponse = $bdd->query('SELECT nom, possesseur, prix FROM jeux_video WHERE possesseur=\'Patrick\' AND prix < 20 ORDER BY prix LIMIT 0, 2');
		    $reponse = $bdd->query('SELECT nom, possesseur, console, prix FROM jeux_video WHERE console=\'Xbox\' OR console=\'PS2\' ORDER BY prix DESC LIMIT 0,10');

		    while ($donnees = $reponse->fetch())
		    {
		        echo '<strong>' . $donnees['nom'] . '</strong> appartient à ' . $donnees['possesseur'] . 'Prix : ' . $donnees['prix'] .'&euro;<br />';
		    }

		    $reponse->closeCursor();
		}
		catch(Exception $e)
		{
			die('Erreur : '.$e->getMessage());
		}
		?>

		<h1>Construire des requêtes en fonction de variables</h1>

		<p><em><strong>A ne pas faire</strong> : concaténer une variable dans une requête</em></p>

		<h2>Les requêtes préparées</h2>
		<p><strong>Avec des marqueurs "?"</strong></p>
		<ul>
			<li><a href="10.Mysql.php?possesseur=Michel&amp;prix_max=20">10.Mysql.php?possesseur=Michel&prix_max=20</a></li>
			<li><a href="10.Mysql.php?possesseur=Michel&amp;prix_max=40">10.Mysql.php?possesseur=Michel&prix_max=40</a></li>
			<li><a href="10.Mysql.php?possesseur=Patrick&amp;prix_max=45">10.Mysql.php?possesseur=Patrick&prix_max=45</a></li>
			<li><a href="10.Mysql.php?possesseur=Florent&amp;prix_max=15">10.Mysql.php?possesseur=Florent&prix_max=15</a></li>
		</ul>

		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// Astuce pour que tout le reste s'affiche en UTF-8.
			$bdd->query("SET NAMES 'utf8'");
			
			$req = $bdd->prepare('SELECT nom, prix FROM jeux_video WHERE possesseur = ? AND prix <= ? ORDER BY prix');
			$req->execute(array($_GET['possesseur'], $_GET['prix_max']));

		    echo '<ul>';
		    while ($donnees = $req->fetch())
		    {
		        echo '<li>' . $donnees['nom'] . ' (' . $donnees['prix'] . ' EUR)</li>';
		    }
		    echo '</ul>';

		    $reponse->closeCursor();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

		<p><strong>avec des marqueurs nominatifs</strong></p>
		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// Astuce pour que tout le reste s'affiche en UTF-8.
			$bdd->query("SET NAMES 'utf8'");
			
			$req = $bdd->prepare('SELECT nom, prix FROM jeux_video WHERE possesseur = :possesseur AND prix <= :prixmax ORDER BY prix');
			$req->execute(array('possesseur' => $_GET['possesseur'], 'prixmax' => $_GET['prix_max']));

		    echo '<ul>';
		    while ($donnees = $req->fetch())
		    {
		        echo '<li>' . $donnees['nom'] . ' (' . $donnees['prix'] . ' EUR)</li>';
		    }
		    echo '</ul>';

		    $reponse->closeCursor();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


    </body>
</html>