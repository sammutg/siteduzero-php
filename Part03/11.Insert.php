<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert, Update, delete</title>
    </head>
    <body>
	
		<h1>Insert, Update, Delete</h1>

		<h2>INSERT : ajouter des données</h2>

	<!-- ============================================= -->
	<!-- = La requête INSERT INTO et application php = -->
	<!-- ============================================= -->
		<p><strong>exec()</strong> INSERT INTO</p>
		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

		    // On ajoute une entrée dans la table jeux_video
		    // $bdd->exec('INSERT INTO jeux_video(ID, nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(\'\', \'Battlefield 1942\', \'Patrick\', \'PC\', 45, 50, \'2nde guerre mondiale\')');
			// Plus simple
			// $bdd->exec('INSERT INTO jeux_video(nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(\'Battlefield 1942\', \'Patrick\', \'PC\', 45, 50, \'2nde guerre mondiale\')');
			// Encore plus simple
			// $bdd->exec('INSERT INTO jeux_video VALUES(\'\', \'Battlefield 1942\', \'Patrick\', \'PC\', 45, 50, \'2nde guerre mondiale\')');

		    echo 'Le jeu a bien été ajouté !';

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


	<!-- =============================================================== -->
	<!-- = INSERTION DE DONNÉES VARIABLES GRÂCE À UNE REQUÊTE PRÉPARÉE = -->
	<!-- =============================================================== -->
		<p><strong>Insertion de données variables grâce à une requête préparée</strong></p>

		<?php
		try
		{
			// Connexion MySQL sécurisée
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			// entrée à ajouter
			$nom = 'test';
			$possesseur = 'Marc';
			$console = 'xbox';
			$prix = 45;
			$nbre_joueurs_max = 2;
			$commentaires = 'test';

			$req = $bdd->prepare('INSERT INTO jeux_video(nom, possesseur, console, prix, nbre_joueurs_max, commentaires) VALUES(:nom, :possesseur, :console, :prix, :nbre_joueurs_max, :commentaires)');
			$req->execute(array(
				'nom' => $nom,
				'possesseur' => $possesseur,
				'console' => $console,
				'prix' => $prix,
				'nbre_joueurs_max' => $nbre_joueurs_max,
				'commentaires' => $commentaires
				));

			echo 'Le jeu a bien été ajouté !';

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>
		

    </body>
</html>