<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Blog</title>
    </head>
    <body>

		<?php
		try
		{
			require ("12.Connexion.php");

			// 1. RÉCUPÉRATION DU BILLET
			$req = $bdd->prepare('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y\') AS date_creation FROM zero_billet WHERE id=?');
			$req->execute(array($_GET['billet']));

		    // On affiche les infos du billet
		    $donnees = $req->fetch();
		    ?>
				<h1>
					<?php echo $donnees['titre']; ?>
					<br />
					<small>Le <strong><?php echo $donnees['date_creation']; ?></strong></small>
				</h1>
				<p><?php echo nl2br(htmlspecialchars($donnees['contenu'])); ?></p>
			<?php
			$req->closeCursor(); // IMPORTANT Termine le traitement de la requête et libère le curseur pour la prochaine requête

		 	// 2. RÉCUPÉRATION DES COMMENTAIRES
			echo "<h2>Vos commentaires</h2>";
			$req = $bdd->prepare('SELECT auteur, commentaire, DATE_FORMAT(date_commentaire, \'%d/%m/%Y à %Hh%i\') AS date_commentaire FROM zero_commentaire WHERE id_billet = ? ORDER BY date_commentaire');
			$req->execute(array($_GET['billet']));

		    // On affiche les infos pour chaque commentaire
			echo "<ol>";
			while ($donnees = $req->fetch())
			{
			?>
				<li>
				<p>
					 le <?php echo $donnees['date_commentaire']; ?>
					par <strong><?php echo htmlspecialchars($donnees['auteur']); ?></strong>
				</p>
				<p>
					<?php echo nl2br(htmlspecialchars($donnees['commentaire'])); ?>
				</p>
				</li>
			<?php
			} // Fin de la boucle des commentaires
			$req->closeCursor();
			echo "</ol>";

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

		<?php  
		?>

		<p><a href="15.Blog.php">Retour à la liste des billets</a></p>

    </body>
</html>