<?php
// Création d'un cookie pseudo
$sauvegarde_pseudo = $_POST['pseudo'];
setcookie('pseudo', $sauvegarde_pseudo, time() + 60, null, null, false, true);

// Redirection du visiteur vers la page du minichat
header('Location: 12.Minichat3.php');
 ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Chat</title>
    </head>
    <body>

<!-- ========================================== -->
<!-- = Page d'insertion des infos dans la Bdd = -->
<!-- ========================================== --> 
		<?php
		try
		{
			require ("12.Connexion.php");

		    // Insertion du message à l'aide d'une requête préparée
			$req = $bdd->prepare('INSERT INTO minichat(pseudo, message) VALUES(:pseudo, :message)');
			$req->execute(array(
				'pseudo' => $_POST['pseudo'],
				'message' => $_POST['message'],
				));

		
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>