<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>un Mini-Chat</title>
    </head>
    <body>
	
		<h1>TP : Mini-Chat</h1>

		<form action="12.Minichat4.php" method="post">
			<fieldset id="" class="">
				<p>
					<label for="pseudo">Pseudo</label>

					<?php
					// TEST DU PSEUDO
					// Si il existe on affiche une valeur dans le champs texte
					if(isset($_COOKIE['pseudo']))
					{
					?>
						
						<input type="text" name="pseudo" id="pseudo" value="<?php echo $_COOKIE['pseudo']; ?>" />
					<?php
					}
					else // Sinon on affiche un champ vide
					{
					?>
						<input type="text" name="pseudo" id="pseudo" />
					<?php
					}
					?>
				</p>
				<p>
					<label for="message">Votre message</label>
					<textarea name="message" id="message" rows="8" cols="45"></textarea>
				</p>
			</fieldset>
			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

		<h2>Les 10 derniers messages</h2>
		<a href="12.Minichat3.php">Rafraîchir les messages</a>

		<?php
		try
		{
			require ("12.Connexion.php");

		    // On récupère tout le contenu de la table minichat
		    $reponse = $bdd->query('SELECT * FROM minichat ORDER BY ID DESC LIMIT 0,10');

		    // On affiche chaque entrée une à une
		    while ($donnees = $reponse->fetch())
		    {
		    ?>
			<p>
				<strong><?php echo htmlspecialchars($donnees['pseudo']); ?></strong> : 
				<em><?php echo htmlspecialchars($donnees['message']); ?></em>
			</p>
		    <?php
		    }

		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

		<a href="12.Minichat5.php">Afficher tous les messages</a>

    </body>
</html>