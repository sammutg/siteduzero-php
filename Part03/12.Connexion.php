<?php

// Connexion MySQL sécurisée
$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

// Astuce pour que tout s'affiche en UTF-8.
$bdd->query("SET NAMES 'utf8'");

?>