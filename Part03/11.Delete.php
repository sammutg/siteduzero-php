<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insert, Update, delete</title>
    </head>
    <body>
	
		<h1>Insert, Update, Delete</h1>

		<h2>DELETE : supprimer des données</h2>

		<p>Cette requête supprimé l'entrée avec le nom = \'Battlefield 1942\'</p>
		<p><strong>exec()</strong> DELETE FROM</p>
		<?php
		try
		{
		    $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			$bdd = new PDO('mysql:host=localhost;dbname=demo_phpzero', 'root', 'root', $pdo_options);

			$nb_modifs = $bdd->exec('DELETE FROM jeux_video WHERE nom = \'Battlefield 1942\'');
			echo $nb_modifs . ' entrée(s) a(ont) été supprimée(s) !';

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

    </body>
</html>