<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Fonctions SQL</title>
    </head>
    <body>

		<h1>Les Fonctions SQL</h1>
		<h2>Les fonctions scalaires</h2>

		<?php
		try
		{
			require ("12.Connexion.php");

			echo "<p><strong>UPPER : convertir en majuscules</strong></p>";
			$reponse = $bdd->query('SELECT UPPER(nom) AS nom_maj FROM jeux_video LIMIT 0,5');
			while ($donnees = $reponse->fetch())
			{
				echo $donnees['nom_maj'] . '<br />';
			}
		    $reponse->closeCursor(); // Termine le traitement de la requête

			echo "<p><strong>LENGTH : compter le nombre de caractères</strong></p>";
			$reponse = $bdd->query('SELECT LENGTH(nom) AS longueur_nom FROM jeux_video LIMIT 0,5');
			while ($donnees = $reponse->fetch())
			{
				echo $donnees['longueur_nom'] . '<br />';
			}
		    $reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

		<hr />
		<h2>Les fonctions d'agrégat</h2>

		<?php
		try
		{
			require ("12.Connexion.php");

		// ===============================
		// = AVG : calcule d'une moyenne =
		// ===============================
			echo "<p><strong>AVG : calcule d'une moyenne</strong></p>";
			echo "<p>La moyenne sur la liste des jeux qui appartiennent à Patrick et qui se jouent à 1 seul joueur est de : ";
			$reponse = $bdd->query('SELECT AVG(prix) AS prix_moyen FROM jeux_video WHERE possesseur=\'Patrick\' AND nbre_joueurs_max=1');

			$donnees = $reponse->fetch(); // Inutile de faire une boucle vu qu'on recoit une seule entrée
			echo '<strong>' . $donnees['prix_moyen'] . '</strong></p>';
		    $reponse->closeCursor(); // Termine le traitement de la requête

		// =================================
		// = SUM : additionner les valeurs =
		// =================================
			echo "<p><strong>SUM : additionner les valeurs</strong></p>";
			echo "<p>Valeur totale des jeux appartenant à Patrick : ";
			$reponse = $bdd->query('SELECT SUM(prix) AS prix_total FROM jeux_video WHERE possesseur=\'Patrick\'');

			$donnees = $reponse->fetch();
			echo '<strong>' . $donnees['prix_total'] . '</strong><br />';
		    $reponse->closeCursor(); // Termine le traitement de la requête

		// ========================================================
		// = MAX - MIN : retourner la valeur maximale ou minimale =
		// ========================================================
			echo "<p><strong>MAX : additionner les valeurs</strong></p>";
			$reponse = $bdd->query('SELECT MAX(prix) AS prix_max FROM jeux_video');

			$donnees = $reponse->fetch();
			echo "<p>Le prix du jeu le plus cher : ";
			echo '<strong>' . $donnees['prix_max'] . '&euro;</strong><br />';
		    $reponse->closeCursor(); // Termine le traitement de la requête

			$reponse = $bdd->query('SELECT MIN(prix) AS prix_min FROM jeux_video');
			$donnees = $reponse->fetch();
			echo "<p>Le prix du jeu le moins cher : ";
			echo '<strong>' . $donnees['prix_min'] . '&euro;</strong><br />';
		    $reponse->closeCursor(); // Termine le traitement de la requête

		// =======================================
		// = COUNT : compter le nombre d'entrées =
		// =======================================
			echo "<p><strong>COUNT : compter le nombre d'entrées</strong></p>";
			$reponse = $bdd->query('SELECT COUNT(*) AS nbjeux FROM jeux_video');
			$donnees = $reponse->fetch();
			echo "<p>Le nombre total de jeux est : "  . $donnees['nbjeux'];
			$reponse->closeCursor(); // Termine le traitement de la requête

			$reponse = $bdd->query('SELECT COUNT(possesseur) AS nbpossesseurs FROM jeux_video WHERE possesseur=\'Florent\'');
			$donnees = $reponse->fetch();
			echo "<p>Le nombre de jeux que possèdent Florent : ";
			echo '<strong>' . $donnees['nbpossesseurs'] . '</strong><br />';
			$reponse->closeCursor(); // Termine le traitement de la requête

			$reponse = $bdd->query('SELECT COUNT(nom) AS totaljeux FROM jeux_video WHERE console=\'NES\' AND possesseur=\'Florent\'');
			$donnees = $reponse->fetch();
			echo "<p>Le nombre de jeux NES que possèdent Florent : ";
			echo '<strong>' . $donnees['totaljeux'] . '</strong><br />';
			$reponse->closeCursor(); // Termine le traitement de la requête

			$reponse = $bdd->query('SELECT COUNT(DISTINCT possesseur) AS nbpossesseurs FROM jeux_video');
			$donnees = $reponse->fetch();
			echo "<p>Le nombre de personnes qui possèdent des jeux : ";
			echo '<strong>' . $donnees['nbpossesseurs'] . ' personnes</strong><br />';
			$reponse->closeCursor(); // Termine le traitement de la requête

		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>

		<hr />
		<h2>le groupement de données</h2>

		<?php
		try
		{
			require ("12.Connexion.php");

		// ==================================
		// = GROUP BY : grouper des données =
		// ==================================
			echo "<p><strong>GROUP BY : grouper des données</strong></p>";
			echo "<p><strong>Prix moyen des jeux</strong></p> <ul>";
			$reponse = $bdd->query('SELECT AVG(prix) AS prix_moyen, console FROM jeux_video GROUP BY console');
			while ($donnees = $reponse->fetch())
			{
				echo '<li><strong>' . $donnees['console'] . '</strong> : ' . round($donnees['prix_moyen'], 2) . '&euro;</li>';
			}
		    $reponse->closeCursor(); // Termine le traitement de la requête
			echo "</ul>";

		// ==================================
			echo "<p><strong>Valeur totale des jeux par chaque personnes</strong></p><ul>";
			$reponse = $bdd->query('SELECT COUNT(possesseur) AS nb_jeux, possesseur FROM jeux_video GROUP BY possesseur');
			while ($donnees = $reponse->fetch())
			{
				echo '<li><strong>' . $donnees['possesseur'] . '</strong> : ' . $donnees['nb_jeux'] . '</li>';
			}
		    $reponse->closeCursor(); // Termine le traitement de la requête
			echo "</ul>";

			// ==================================
			echo "<p><strong>HAVING : filtrer les données regroupées</strong></p>";
			echo "<p>On récupère la liste des consoles et leur prix moyen si ce prix moyen ne dépasse pas 10 euros.</p><ul>";
			$reponse = $bdd->query('SELECT AVG(prix) AS prix_moyen, console FROM jeux_video GROUP BY console HAVING prix_moyen <= 10');
			while ($donnees = $reponse->fetch())
			{
				echo '<li><strong>' . $donnees['console'] . '</strong> : ' . round($donnees['prix_moyen'], 2) . '&euro;</li>';
			}
		    $reponse->closeCursor(); // Termine le traitement de la requête
			echo "</ul>";


		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		?>


    </body>
</html>