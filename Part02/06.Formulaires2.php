<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Formulaires</title>
    </head>
    <body>
	
		<h2>Récupération des données envoyées par les Formulaires</h2>
		<?php

			echo '<pre>';
			print_r($_POST);
			echo '</pre>';

			// 1 On liste les éléments envoyés par le formulaire
			echo 'Votre prénom est <em>' . $_POST['prenom'] . '</em><br />';
			echo 'Votre choix : <em>' . $_POST['choix'] . '</em><br />';
			echo 'Vous aimez les frites : <em>' . $_POST['frites'] . '</em><br />';
			echo 'Votre message <br /> <cite>: ' . $_POST['message'] . '</cite><br />';
			
			// 2 Affiche des choses différentes si la case est cochée ou pas
			if (isset($_POST['case'])) // On a coché la case
			{
				echo 'Vous avez coché la case !';
			}
			else // Il manque le paramètres
			{
				echo 'Vous n\'avez pas coché la case !';
			}
		?>

		<p><strong>Eviter la faille XSS</strong></p>
		<p>On résout le problème en protégeant le code HTML.
		<br />Il suffit d'échapper le code HTML en utilisant la fonction <strong>htmlspecialchars</strong> (code les chevrons html)</p>
		<?php 
			echo 'Votre prénom est <em>' . htmlspecialchars($_POST['prenom']) . '</em><br />';
		?>
		<p>ou <strong>strip_tags</strong> pour retirer toutes les balises html</p>
		<?php 
			echo 'Votre prénom est <em> ' . strip_tags($_POST['prenom']) . '</em><br />';
		 ?>

		<hr />
		<h2>Envoi de Fichier</h2>
		<?php
		
		// 1 Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
		if (isset($_FILES['monfichier']) AND $_FILES['monfichier']['error'] == 0)
		{

			// 2 Testons si le fichier n'est pas trop gros. Interdit les fichiers de plus de 1 Mo
	        if ($_FILES['monfichier']['size'] <= 1000000)
	        {
			
				// 3 Testons si l'extension est autorisée
				// pathinfo renvoie un array contenant entre autres l'extension du fichier dans $infosfichier['extension']
				$infosfichier = pathinfo($_FILES['monfichier']['name']);
				$extension_upload = $infosfichier['extension'];
				$extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
				
				if (in_array($extension_upload, $extensions_autorisees))
				{
					// 4. Validation de l'upload
					// On peut valider le fichier et le stocker définitivement
					move_uploaded_file($_FILES['monfichier']['tmp_name'], 'uploads/' . basename($_FILES['monfichier']['name']));
					echo "L'envoi a bien été effectué !";
				} // eof 3
				else 
				{
					echo 'une erreur est parvenue à l\'autorisation ou à l\'upload du fichier';
					echo $_FILES['monfichier']['tmp_name'];
				}

	        } // eof 2
			else 
			{
				echo 'Le fichier est trop volumineux'; 
			}
			
		} // eof 1
		else 
		{
			echo 'Le fichier n\'a pas été envoyé'; 
		}

		?>
		<br /><br />
		<a href="05.Formulaires.php">Pour le modifier reviens sur le formulaire</a>

    </body>
</html>