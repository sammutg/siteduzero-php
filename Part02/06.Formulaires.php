<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Formulaires</title>
    </head>
    <body>
	
		<h1>Les Formulaires</h1>
		<h2>Base de formulaire</h2>

		<form method="post" action="06.Formulaires2.php">
			<input type="hidden" name="nomdusite" value="mon site perso" />

			<fieldset>
				<legend>Veuillez renseigner les infos suivantes :</legend>
				<p>
					<label for="prenom">Votre prénom</label>
					<input type="text" name="prenom" id="prenom" />
				</p>
				<p>
	 				<label for="choix">Votre choix</label>
	 				<select name="choix" id="choix">
	 				    <option value="choix1">Choix 1</option>
	 				    <option value="choix2" selected="selected">Choix 2</option>
	 				    <option value="choix3">Choix 3</option>
	 				    <option value="choix4">Choix 4</option>
	 				</select>
				</p>
				<p>
					Aimez-vous les frites ?
					<input type="radio" name="frites" value="oui" id="oui" checked="checked" /> 
					<label for="oui">Oui</label>
					<input type="radio" name="frites" value="non" id="non" />
					<label for="non">Non</label>
				</p>
				<p>
					<label for="message">Votre message</label>
					<textarea name="message" id="message" rows="8" cols="45"></textarea>
				</p>

				<input type="checkbox" name="case" id="case" checked="checked"/>
				<label for="case">Ma case à cocher</label>

			</fieldset>

			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

		<h2>Formulaire d'envoi de fichier</h2>
		<form action="05.Formulaires2.php" method="post" enctype="multipart/form-data">
			<fieldset id="" class="">
				<legend>Formulaire d'envoi de fichier</legend>
				<p>
					<label for="monfichier">Votre fichier</label>
					<input type="file" name="monfichier" /><br />
				</p>
			</fieldset>
			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

    </body>
</html>