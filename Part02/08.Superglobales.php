<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
 
// On s'amuse à créer quelques variables de session dans $_SESSION
$_SESSION['prenom'] = 'Jean';
$_SESSION['nom'] = 'Dupont';
$_SESSION['age'] = 24;

// Création d'un cookie pseudo
setcookie('pseudo', 'Guillaume', time() + 60, null, null, false, true);
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Variables superglobales, sessions et cookies</title>
    </head>
    <body>
	
		<h1>Variables superglobales, sessions et cookies</h1>

		<h2>Variables superglobales</h2>

		<p>$_COOKIE</p>
		<?php
		echo '<pre>';
		print_r($_COOKIE);
		echo '</pre>';
		?>

		<p>$_SERVER</p>
		<?php
		echo '<pre>';
		print_r($_SERVER);
		echo '</pre>';
		?>

		<hr />
		<h2>Les sessions</h2>
	    <p>
	        Salut <?php echo $_SESSION['prenom']; ?> !<br />
	        Tu es à l'accueil de mon site (index.php). Va sur la page <a href="08.Superglobales2.php">Superglobales2</a> ?
	    </p>

		<h2>Les cookies</h2>
		<p>
		    Hé ! Je me souviens de toi !<br />
		    Tu t'appelles <?php echo $_COOKIE['pseudo']; ?> ?
		</p>

    </body>
</html>