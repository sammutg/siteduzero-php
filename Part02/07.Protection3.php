<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Page protégée par mot de passe</title>
    </head>
    <body>

		<h1>TP : protection par mot de passe</h1>

		<?php
		$codesecret = 'kangourou';

		// SI le Mot de passe n'existe pas ou si le mot de passe est incorect
		if (!isset($_POST['mot_de_passe']) OR $_POST['mot_de_passe'] != $codesecret)
		{
		// ALORS On affiche le formulaire
		?>
		
			<p>Cette page est réservée au personnel de la NASA. Si vous ne travaillez pas à la NASA, inutile d'insister vous ne trouverez jamais le mot de passe ! ;-)</p>
			<form method="post" action="07.Protection3.php">

				<fieldset>
					<legend>Veuillez renseigner le mot de passe :</legend>
					<p>
						<label for="mot_de_passe">Le mot de passe</label>
						<input type="password" name="mot_de_passe" id="mot_de_passe" />
					</p>

				</fieldset>
				<?php
				if (isset($_POST['mot_de_passe']) AND $_POST['mot_de_passe'] != $codesecret) 
				{
					echo "<p>Mot de passe incorrect</p>";
				}
				?>
				<p><input type="submit" value="Continue &rarr;"></p>			
			</form>

		<?php
			}
			else // SINON le mot de passe existe ou est bon
			{
		?>
				<h1>Voici les codes d'accès :</h1>
				<p><strong>CRD5-GTFT-CK65-JOPM-V29N-24G1-HH28-LLFV</strong></p>   

				<p>
				Cette page est réservée au personnel de la NASA. N'oubliez pas de la visiter régulièrement car les codes d'accès sont changés toutes les semaines.<br />
				La NASA vous remercie de votre visite.
				</p>
		<?php 
		}
		?>

    </body>
</html>