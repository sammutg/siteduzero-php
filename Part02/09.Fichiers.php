<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lire et écrire dans un fichier</title>
    </head>
    <body>
	
		<h1>Lire et écrire dans un fichier</h1>
		<?php
		// 1 : on ouvre le fichier
		$monfichier = fopen('09.Fichiers.txt', 'r+');

		// 2 : on fera ici nos opérations sur le fichier...
		$pages_vues = fgets($monfichier); // On lit la première ligne (nombre de pages vues)
		$pages_vues++; // On augmente de 1 ce nombre de pages vues
		fseek($monfichier, 0); // On remet le curseur au début du fichier
		fputs($monfichier, $pages_vues); // On écrit le nouveau nombre de pages vues

		// 3 : quand on a fini de l'utiliser, on ferme le fichier
		fclose($monfichier);
		
		echo '<p>Cette page a été vue ' . $pages_vues . ' fois !</p>';
		
		?>

		</pre>
    </body>
</html>