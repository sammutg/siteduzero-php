<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Page protégée par mot de passe</title>
    </head>
    <body>

		<h1>TP : protection par mot de passe</h1>

		<p>Cette page est réservée au personnel de la NASA. Si vous ne travaillez pas à la NASA, inutile d'insister vous ne trouverez jamais le mot de passe ! ;-)</p>
		<form method="post" action="07.Protection2.php">

			<fieldset>
				<legend>Veuillez renseigner le mot de passe :</legend>
				<p>
					<label for="mot_de_passe">Le mot de passe</label>
					<input type="password" name="mot_de_passe" id="mot_de_passe" />
				</p>
			</fieldset>

			<p><input type="submit" value="Continue &rarr;"></p>
		</form>

    </body>
</html>