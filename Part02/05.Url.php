<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Urls</title>
    </head>
    <body>
	
		<h1>Transmettre des paramètres dans l'Url</h1>
		<p>page.php<strong>?</strong>param1=valeur1<strong>&</strong>param2=valeur2<strong>&</strong>param3=valeur3<strong>&</strong>param4=valeur4…</p>

		<p><a href="05.Url2.php?nom=Dupont&amp;prenom=Jean&repeter=8">Dis-moi bonjour !</a></p>

    </body>
</html>