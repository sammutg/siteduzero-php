<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Urls</title>
    </head>
    <body>
	
		<h1>Récupération des paramètres dans l'Url</h1>
		<p>Bonjour <?php echo $_GET['prenom'] . ' ' . $_GET['nom']; ?> !</p>


		<h2><strong>isset</strong></h2>
		<p>Teste la présence d'un paramètre</p>

		<?php
		if (isset($_GET['prenom']) AND isset($_GET['nom'])) // On a le nom et le prénom
		{
			echo 'Tous les paramètres sont bien renseignés !';
		}
		else // Il manque des paramètres, on avertit le visiteur
		{
			echo 'Il faut renseigner un nom et un prénom !';
		}
		?>

		<h2>Contrôler la valeur des paramètres</h2>
		<p><strong>Avec une Boucle For</strong></p>
		<?php
		if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter'])) // On a le nom et le prénom et le paramètre repeter
		{
			for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
			{
				echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
			}
		}
		else // Il manque des paramètres, on avertit le visiteur
		{
			echo 'Il faut renseigner un nom et un prénom et le nombre de répétition !';
		}
		?>

		<p><strong>Avec une Boucle While</strong></p>
		<?php
		$test = 0;
		
		if (isset($_GET['prenom']) AND isset($_GET['nom']) AND isset($_GET['repeter'])) // On a le nom et le prénom et le paramètre repeter
		{
			while ($test < $_GET['repeter'])
			{
				echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
				$test++;
			}
		}
		else // Il manque des paramètres, on avertit le visiteur
		{
			echo 'Il faut renseigner un nom et un prénom et le nombre de répétition !';
		}
		?>

		<p><strong>En testant le paramètre répéter</strong></p>
		<?php
		// 1 : On force la conversion en nombre entier
		$_GET['repeter'] = (int) $_GET['repeter'];
		echo 'Nombre de répétition : ' . $_GET['repeter'] . '<br />';

		// 2 : Le nombre doit être compris entre 1 et 10
		if ($_GET['repeter'] <= 10)
		{
			for ($i = 0 ; $i < $_GET['repeter'] ; $i++)
			{
				echo 'Bonjour ' . $_GET['prenom'] . ' ' . $_GET['nom'] . ' !<br />';
			}
		}
		else 
		{
		   echo 'Il faut renseigner un nom, un prénom et un nombre de répétitions inférieur ou égal à 10 !';
		}
		?>

    </body>
</html>