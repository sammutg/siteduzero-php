<?php
// On démarre la session AVANT d'écrire du code HTML
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Variables superglobales, sessions et cookies</title>
    </head>
    <body>
	
		<h1>Variables superglobales, sessions et cookies</h1>

		<h2>Les sessions</h2>
	    <p>Re-bonjour !</p>
	    <p>
	        Je me souviens de toi ! Tu t'appelles <?php echo $_SESSION['prenom'] . ' ' . $_SESSION['nom']; ?> !<br />
	        Et ton âge hummm... Tu as <?php echo $_SESSION['age']; ?> ans, c'est ça ? :-D
	    </p>
	
		<h2>Les cookies</h2>
		<p>
		    Hé ! Je me souviens de toi !<br />
		    Tu t'appelles <?php echo $_COOKIE['pseudo']; ?> ?
		</p>

		</pre>
    </body>
</html>