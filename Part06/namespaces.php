<?php
/**
 * Déclaration du namespace
*/

		namespace MonNamespace;
		// Déclaration du namespace.
		// Toutes les constantes, fonctions et classes déclarées ici feront partie du namespace MonNamespace.
			function strlen()
			{
					echo 'Hello world !';
			} 

/**
 * Déclaration de plusieurs namespaces. Ne marche pas sur ma config
*/

		namespace A;
		function quelNamespace()
		{
			echo "Mon namspace est ". __NAMESPACE__ ."<br />";
		}
		namespace B;
		function quelNamespace()
		{
			echo "Mon namspace est ". __NAMESPACE__ ."<br />";
		}
		namespace A\B;

?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Les namespaces / Les espaces de noms en PHP</title>
	</head>
	<body>

		<h1>Déclarer un namespace</h1>
		<h2>Fonction du namespace MonNamespace</h2>
		<p>
			<?php 
					namespace MonNamespace;
					strlen(); 
			?>
		</p>
		
		<h2>Fonction du namespace global</h2>
		<p><?php echo \strlen('Hello world !'); ?></p>

		<h2>Déclaration de plusieurs namespaces dans un même fichier</h2>
		<p>
			<?php 
				namespace B;
				quelNamespace();
			?>
		</p>

		<h2>Déclaration de sous-namespaces</h2>
		<?php 
			namespace A\B;
			echo __NAMESPACE__; 
		?>

		<h2>Accéder à un élément d'un namespace différent</h2>
		<?php
			namespace A\B;
			function getNamespace()
			{
					echo __NAMESPACE__;
			}
		?>

		<?php
			namespace A;
			{
					echo "<p>Appel de façon relative.</p>";
					B\getNamespace(); // Appel de façon relative.
					echo "<p>Appel de façon absolue.</p>";
					\A\B\getNamespace(); // Appel de façon absolue.
			}
		?>

		<h2>Créer des alias</h2>
		<code>use A\B\C\D\E\F as nsF;</code>
		<p>
			<?php
				require 'f.ns.php';
				use A\B\C\D\E\F as nsF;
				nsF\getNamespace(); // Se transforme en A\B\C\D\E\F\getNamespace().
			?>
		</p>

		<p>
			…revient au même que d'écrire :
			<code>use A\B\C\D\E\F;</code>
		</p>
		<?php 
			use A\B\C\D\E\F;
			F\getNamespace(); // Se transforme en A\B\C\D\E\F\getNamespace().
		?>
		<h2>Importation de classes</h2>
		<?php	
				require 'f.ns.php';
				use A\B\C\D\E\F\MaClasse;

				$a = new MaClasse; // Se transforme en $a = new A\B\C\D\E\F\MaClasse.
				$a->hello();
		?>

		<?php var_dump($a); ?>


	</body>
</html>