<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head>
<title>Les Variables</title>
</head>
<body>

	<h1>les types de donn�es</h1>

	<h2>Le type string (cha�ne de caract�res)</h2>
	<?php
		//$variable = "Mon \"nom\" est Mateo21";
		// $variable = 'Je m\'appelle Mateo21';
		//$variable = 'Mon "nom" est Mateo21';
		$variable = "Je m'appelle Mateo21";
		echo $variable . "<br/>\n";
	?>
	
	<h2>Le type int (nombre entier)</h2>
	<?php
		$age_du_visiteur = 17;
		echo "L'age du visiteur est de " . $age_du_visiteur . "<br/>\n";
	?>
	
	<h2>Le type float (nombre d�cimal)</h2>
	<?php
		$poids = 86.3;
		echo "Mon poids est de " . $poids . "<br/>\n";
	?>

	<h2>Le type bool (bool�en)</h2>
	<?php
		$je_suis_un_zero = true;
		$je_suis_bon_en_php = false;
		echo "Je suis un z�ro ? " . $je_suis_un_zero . "<br/>\nJe suis bon en PHP ? " . $je_suis_bon_en_php . "<br/>\n";
	?>

	<h2>Une variable vide avec NULL</h2>
	<?php
		$pas_de_valeur = NULL;
		echo "Valeur " . $pas_de_valeur . "<br/>\n";
	?>

	<h1>La concat�nation</h1>
	<?php
		echo "Variable directement dans la ligne : $age_du_visiteur ans <br/>\n";
	?>

	<?php
		echo 'L\'�ge du visiteur est de ' . $age_du_visiteur . ' ans<br/>';
	?>

	<h1>Calculs</h1>
	<?php
		// $nombre = 2 + 4; // $nombre prend la valeur 6
		// $nombre = 5 - 1; // $nombre prend la valeur 4
		// $nombre = 3 * 5; // $nombre prend la valeur 15
		// $nombre = 10 / 2; // $nombre prend la valeur 5
	
		// Allez on rajoute un peu de difficult�
		 $nombre = 3 * 5 + 1; // $nombre prend la valeur 16
		// $nombre = (1 + 2) * 2; // $nombre prend la valeur 6

		echo $nombre;
	?>
	
	<h2>Le modulo</h2>
	<?php
		// $nombre = 10 % 5; // $nombre prend la valeur 0 car la division tombe juste
		$nombre = 10 % 3; // $nombre prend la valeur 1 car il reste 1
		echo 'Le modulo de 10/3 est : ' . $nombre . '<br /> On "rentre" 3 fois 3 dans 10 et il reste ' . $nombre;
	?>

	<h1>Les variables variables</h1>

	<?php
	$afficher = 'pays'; // Modifiez la valeur de $afficher pour voir...

	// On d�finit les 3 variables dont on a parl�
	$ville = 'Marseille';
	$pays = 'France';
	$continent = 'Europe';

	echo ${$afficher}; // On affiche la variable dont le nom est "ville" dans notre exemple
	?>

</body>
</html>