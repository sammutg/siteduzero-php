<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Fonctions</title>
    </head>
    <body>
	
		<h1>Les Fonctions</h1>

		<h2>Les fonctions php</h2>

		<h3>strlen</h3>
		<?php
		$phrase = 'Bonjour les Zéros ! Je suis une phrase !';
		$longueur = strlen($phrase);
		
		echo 'La phrase ci-dessous comporte ' . $longueur . ' caractères :<br />' . $phrase;
		?>

		<h3>str_replace</h3>
		
		<?php
		$search = "b";
		$replace = "p";
		$subject = "bim bam bom boum";
		
		$ma_variable = str_replace($search, $replace, $subject);

		echo $subject . ' après transformation ' . $ma_variable;
		?>

		<h3>date</h3>
		
		<?php
		$annee = date('Y');
		echo $annee;
		?>
		
		<p><strong>Date complète avec l'heure</strong></p>
		
		<?php
		// Enregistrons les informations de date dans des variables

		$jour = date('d');
		$mois = date('m');
		$annee = date('Y');
		$heure = date('H');
		$minute = date('i');

		// Maintenant on peut afficher ce qu'on a recueilli
		echo 'Bonjour ! Nous sommes le ' . $jour . '/' . $mois . '/' . $annee . 'et il est ' . $heure. ' h ' . $minute;
		?>
		
		<h2>Ses propres fonctions</h2>
		<?php
			// Fonction pour dire Bonjour
			function DireBonjour ($nom)
			{
				echo 'Bonjour ' . $nom . ' !<br />';
			}

			DireBonjour('Guillaume');
		?>
		
		<p><strong>Calcul volume d'un cône</strong></p>

		<?php

		$rayon = 5;
		$hauteur = 2;
		$pi = 3.14;

		// Volume du rayon
		$volume = $rayon * $rayon * $pi * 2 * (1/3);
			
		echo "Volume d'un cône de rayon $rayon cm et hauteur $hauteur cm = $volume cm<sup>3</sup> !<br />";
		?>
		
		<p>la fonction qui calcule le volume du cône</p>
		
		<?php
		// Ci-dessous, la fonction qui calcule le volume du cône
		function VolumeCone($rayon, $hauteur)
		{
		   $volume = $rayon * $rayon * 3.14 * $hauteur * (1/3); // calcul du volume
		   return $volume; // indique la valeur à renvoyer, ici le volume
		}

		$volume = VolumeCone(3, 1);
		echo 'Le volume d\'un cône de rayon 3 et de hauteur 1 est de ' . $volume;
		?>

    </body>
</html>