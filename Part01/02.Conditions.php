<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Conditions</title>
    </head>
    <body>
	
		<h1>Conditions</h1>

		<h2>Les symboles à connaître</h2>
		<ul>
			<li><strong>==</strong>	Est égal à</li>
			<li><strong>></strong>	Est supérieur à</li>
			<li><strong><</strong>	Est inférieur à</li>
			<li><strong>>=</strong>	Est supérieur ou égal à</li>
			<li><strong><=</strong>	Est inférieur ou égal à</li>
			<li><strong>!=</strong>	Est différent de</li>
		</ul>

		<h2>If else</h2>
	
		<?php
		$age = 11;
		$sexe = "extraterrestre";

		if ($age <= 12) // SI l'âge est inférieur ou égal à 12
		{
		    echo "Salut gamin ! Bienvenue sur mon site !<br />";
		    // $autorisation_entrer = "oui";
		    $autorisation_entrer = true;
		}
		else // SINON
		{
		    echo "Ceci est un site pour enfants, vous êtes trop vieux pour pouvoir entrer. Au revoir !<br />";
		    //	$autorisation_entrer = "non";
		    $autorisation_entrer = false;
		}
			echo "Avez-vous l'autorisation d'entrer ? La réponse est $autorisation_entrer";
		?>

		<h2>If elseif else</h2>
		<p><small>Attention au majuscule dans les valeurs des variables !</small></p>
		
		<?php
		if ($autorisation_entrer == "oui") // SI on a l'autorisation d'entrer
		{
		    // instructions à exécuter quand on est autorisé à entrer
		    echo "Tu es un gamin va jouer ailleurs";
		}
		elseif ($autorisation_entrer == "non") // SINON SI on n'a pas l'autorisation d'entrer
		{
		    // instructions à exécuter quand on n'est pas autorisé à entrer
		    echo "Dégage le vieux !";
		}
		else // SINON (la variable ne contient ni Oui ni Non, on ne peut pas agir)
		{
		    echo "Euh, je ne connais pas ton âge, tu peux me le rappeler s'il te plaît ?";
		}
		?>
		
		<h2>Avec des booléens</h2> 
		
		<?php
		if ($autorisation_entrer == true)
		{
		    echo "Bienvenue petit Zéro :o)";
		}
		elseif ($autorisation_entrer == false)
		{
		    echo "T'as pas le droit d'entrer !";
		}
		?>
		
		<p><strong>Simplification</strong></p>

		<p>si <i>$autorisation_entrer</i> vaut <i>true</i> (si on à l'autorisation d'entrer)</p>
		<?php
		if ($autorisation_entrer)
		{
		    echo "Bienvenue petit Zéro :o)";
		}
		else
		{
		    echo "T'as pas le droit d'entrer !";
		}
		?>
		
		<p>Si <i>$autorisation_entrer</i> vaut <i>false</i></p>
		
		<?php
		if (! $autorisation_entrer)
		{
		    echo "v2 : T'as pas le droit d'entrer !";
		}
		else 
		{
		    echo "v2 : Bienvenue petit Zéro :o)";
		}
		?>
		
		<hr />
		<h1>Des conditions multiples</h1>
		
		<h2>AND : &amp;&amp; </h1>
		
		<?php
		
		if ($age <= 12 AND $sexe == "garçon")
		{
		    echo "Bienvenue sur le site de Captain Mégakill !";
		}
		elseif ($age <= 12 && $sexe == "fille")
		{
		    echo "C'est pas un site pour les filles ici, retourne jouer à la Barbie !";
		}
		else
		{
		    echo "Désolé pour toi !";
		}
		?>

		<h2>OR : || </h1>

		<?php
		if ($sexe == "fille" OR $sexe == "garçon")
		{
		    echo "Salut Terrien !";
		}
		else
		{
		    echo "Euh, si t'es ni une fille ni un garçon, t'es quoi alors ? peut-être un $sexe non ?";
		}
		?>

		<h1>Bonus</h1> 

		<?php
		$numero_mystere = 1;
		
		if ($numero_mystere == 1)
		{
		    echo '<strong>Bravo !</strong> Vous avez trouvé le nombre mystère !<br />';
		}
		?>
		
		
		<?php
		if ($numero_mystere == 1)
		{
		?>
		<strong>Bravo !</strong> Vous avez trouvé le nombre mystère !
		<?php
		}
		?>


		<h1>Switch</h1>

		<p><strong>structure if elseif else</strong></p> 
		<?php
		$note = 16;
		
		echo "Ma note est de $note/20<br />\n";
		
		if ($note == 0)
		{
		    echo "Tu es vraiment un gros Zéro !!!";
		}

		elseif ($note == 5)
		{
		    echo "Tu es très mauvais";
		}

		elseif ($note == 7)
		{
		    echo "Tu es mauvais";
		}

		elseif ($note == 10)
		{
		    echo "Tu as pile poil la moyenne, c'est un peu juste...";
		}

		elseif ($note == 12)
		{
		    echo "Tu es assez bon";
		}

		elseif ($note == 16)
		{
		    echo "Tu te débrouilles très bien !";
		}

		elseif ($note == 20)
		{
		    echo "Excellent travail, c'est parfait !";
		}

		else
		{
		    echo "Désolé, je n'ai pas de message à afficher pour cette note";
		}
		?>

		<p><strong>Switch</strong> ne peut tester que l'égalité</p>

		<?php
		switch ($note) // on indique sur quelle variable on travaille
		{ 
		case 0: // dans le cas où $note vaut 0
		echo "Tu es vraiment un gros Zér0 !!!";
		break;

		case 5: // dans le cas où $note vaut 5
		echo "Tu es très mauvais";
		break;

		case 7: // dans le cas où $note vaut 7
		echo "Tu es mauvais";
		break;

		case 10: // etc etc
		echo "Tu as pile poil la moyenne, c'est un peu juste...";
		break;

		case 12:
		echo "Tu es assez bon";
		break;

		case 16:
		echo "Tu te débrouilles très bien !";
		break;

		case 20:
		echo "Excellent travail, c'est parfait !";
		break;

		default:
		echo "Désolé, je n'ai pas de message à afficher pour cette note";

		}
		?>



    </body>
</html>