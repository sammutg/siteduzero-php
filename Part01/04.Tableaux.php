<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les tableaux</title>
    </head>
    <body>
	
		<h1>Les tableaux</h1>
		<p>Il s'agit de variables "composées"</p>	

		<h2>Les tableaux numérotés</h2>	

		<p><strong>la fonction array</strong></p>

		<?php
		// La fonction array permet de créer un array
		$prenoms = array ('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');
		?>

		<?php
		$prenoms[] = 'François'; // Créera $prenoms[0]
		$prenoms[] = 'Jean'; // Créera $prenoms[1]
		$prenoms[] = 'Nicole'; // Créera $prenoms[2]

		echo "<pre>";
		print_r($prenoms);
		echo "</pre>";
		
		echo 'Le prénom à la ligne 1 est '.  $prenoms[1];
		?>

		<h2>Tableaux associatifs</h2>

		<?php
		// On crée notre array $coordonnees
		$coordonnees = array (
		    'prenom' => 'François',
		    'nom' => 'Dupont',
		    'adresse' => '3 Rue du Paradis',
		    'ville' => 'Marseille');
		
		echo 'La ville rentrée dans les coordonnées est ' . $coordonnees['ville'];
		
		?>
		
		<hr />
		<h2>Lire un tableau</h2>
		<p><strong>La boucle for</strong></p>
		
		<?php
		// On crée notre array $prenoms
		$prenoms = array ('François', 'Michel', 'Nicole', 'Véronique', 'Benoît');

		// Puis on fait une boucle pour tout afficher :
		for ($numero = 0; $numero < 5; $numero++)
		{
		    echo $prenoms[$numero] . '<br />'; // affichera $prenoms[0], $prenoms[1] etc...
		}
		?>
		
		<p><strong>La boucle foreach</strong></p>
		<p>
			C'est une sorte de boucle <em>for</em> spécialisée dans les tableaux.
			<br />Permet de parcourir les tableaux numérotés ou associatifs.	
		</p>
		

		<p>Tableau des prénoms</p>
		<?php
		foreach($prenoms as $element)
		{
		    echo $element . '<br />'; // affichera $prenoms[0], $prenoms[1] etc...
		}
		?>
		
		<p>Tableau des coordonnées</p>

		<?php
		foreach($coordonnees as $element)
		{
		    echo $element . '<br />';
		}
		?>
		
		
		<p><strong>Récupérer la clé de l'élément</strong></p>
		<?php
		foreach($prenoms as $cle => $element)
		{
		    echo '[' . $cle . '] vaut ' . $element . '<br />';
		}
		?>
		
		<p><strong>print_r</strong></p>
		<?php
		
		echo '<pre>';
		print_r($coordonnees);
		echo '</pre>';
		
		?>
		<hr />
		<h2>Rechercher dans un tableau</h2>

		<p><strong>array_key_exists</strong></p>
		<?php
			if (array_key_exists('nom', $coordonnees))
			{
			    echo 'La clé "nom" se trouve dans les coordonnées !<br />';
			}
			if (array_key_exists('pays', $coordonnees))
			{
			    echo 'La clé "pays" se trouve dans les coordonnées !';
			}
			else
			{
				echo 'La clé "pays" ne se trouve pas dans les coordonnées !';
			}
		?>

		<p><strong>in_array</strong></p>
		<?php
		$fruits = array('Banane', 'Pomme', 'Poire', 'Cerise', 'Fraise', 'Framboise');

		if (in_array('Myrtille', $fruits))
		{
		    echo 'La valeur "Myrtille" se trouve dans les fruits !';
		}
		
		if (in_array('Cerise', $fruits))
		{
		    echo 'La valeur "Cerise" se trouve dans les fruits !';
		}
		?>


		<p><strong>array_search</strong></p>
		<?php

			$position = array_search('Fraise', $fruits);
			echo '"Fraise" se trouve en position ' . $position . '<br />';

			$position = array_search('Banane', $fruits);
			echo '"Banane" se trouve en position ' . $position . '<br />';
		
			$position = array_search('Prune', $fruits);
			echo '"Prune" se trouve en position ' . $position;

		?>

    </body>
</html>