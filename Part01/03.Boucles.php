<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Conditions</title>
    </head>
    <body>
	
		<h1>Les Boucles</h1>

		<p><em>Tant que la condition est remplie, la boucle est réexécutée.</em></p>

		<h2>Boucle While</h2>

		<?php
		$nombre_de_lignes = 1;

		while ($nombre_de_lignes <= 10)
		{
		    echo 'Je ne dois pas regarder les mouches voler quand j\'apprends le PHP.<br />';
		    $nombre_de_lignes++; // $nombre_de_lignes = $nombre_de_lignes + 1
		}
		?>

		<p><strong>Déclinaisons</strong></p>
		<p>La variable augmente à chaque passage dans la boucle</p>
		<?php
		$nombre_de_lignes = 1;

		while ($nombre_de_lignes <= 10)
		{
			echo 'Ceci est la ligne n°' . $nombre_de_lignes . '<br />';
			$nombre_de_lignes++;
		}
		?>

		<h2>Boucle For</h2>

		<p><strong>FOR (initialisation, condition, incrémentation)</strong></p>

		<?php
		for ($nombre_de_lignes = 1; $nombre_de_lignes <= 10; $nombre_de_lignes++)
		{
			echo 'Ceci est la ligne n°' . $nombre_de_lignes . '<br />';
		}
		?>

		<h2>Autre présentation des boucles</h2>
		<ul>
		<?php for ($nombre_de_lignes = 1; $nombre_de_lignes <= 10; $nombre_de_lignes++): ?>
		<li><?php echo 'Ceci est la ligne n°' . $nombre_de_lignes; ?></li>
		<?php endfor ?>
		</ul>

    </body>
</html>